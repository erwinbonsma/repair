﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DefaultGameControl : GameControl
{
	float _robotSpeed;
	string _sceneName;
	int _playerTiles;
	int _repairTiles;

	public static void StartGame(float robotSpeed, string sceneName, int playerTiles=-1, int repairTiles=-1) {
		Debug.Log("Creating default game controller");
		currentGame = new DefaultGameControl(robotSpeed, sceneName, playerTiles, repairTiles);
		SceneManager.LoadSceneAsync(sceneName);
	}

	public DefaultGameControl(float robotSpeed, string sceneName, int playerTiles, int repairTiles) {
		_robotSpeed=robotSpeed;
		_sceneName=sceneName;

		if (playerTiles>0) {
			_playerTiles=playerTiles;
		} else {
			_playerTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
				GameControl.CROSSING + GameControl.TCROSSING;
		}
		if (repairTiles>0) {
			_repairTiles=repairTiles;
		} else {
			_repairTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
				GameControl.CROSSING + GameControl.TCROSSING + GameControl.DEADEND;
		}
	}

	public override string GetMap() {
		return null;
	}

	public override float GetSpeed() {
		return _robotSpeed;
	}

	// Returns the tiles that are available to the player as a bit mask:
	public override int GetAvailableTiles() {
		return _playerTiles;
	}

	// Returns the tiles that are available for grid repair as a bit mask:
	public override int GetRepairTiles() {
		return _repairTiles;
	}

	/////// Methods for changing state / updating progress:

	// Load the next level or go to the end screen / main menu:
	public override void LoadNextMap() {
		currentGame=null;
		SceneManager.LoadSceneAsync("MainMenu");
	}

	public override void CalculateUpdateShowScore(int totalMoves, float timeTaken, bool win, LevelControl levelControl) {
		// The default game controller just calls the ShowEndScreen method from LevelControl:
		levelControl.ShowEndScreen(win);
	}
}
