﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A *Game controller* keeps track of the game progress between different levels / scenes. 
/// It can keep track of score, lives, next level, etc.
/// This is the class that should communicate with the HighScore lists.
/// 
/// It is created by launching a game from a menu screen (possibly: pass in a start level).
/// </summary>
public abstract class GameControl {
	// Helper variables for selecting tile sets:
	public const int EMPTY = 1;
	public const int STRAIGHT = 6;
	public const int CORNER = 0x78; // 1111 << 3
	public const int TCROSSING = 0x780; // 1111 << 7
	public const int CROSSING = 0x800; // 1 << 13
	public const int DEADEND = 0xf000; // 1111 << 14
	public const int VSPLIT = 0xf0000;// 1111 << 18

	public static GameControl currentGame {get; protected set;}

	/////// Information for initializing a level:

	public abstract string GetMap();

	public abstract float GetSpeed();

	// Returns the tiles that are available to the player as a bit mask:
	public abstract int GetAvailableTiles();

	// Returns the tiles that are available for grid repair as a bit mask:
	public abstract int GetRepairTiles();

	/////// Methods for changing state / updating progress:

	// Load the next level or go to the end screen / main menu:
	public abstract void LoadNextMap();

	// Calculates the score, for a given number of moves and time needed.
	// Also updates other state: lives, next level, etc.
	// This might take hardness, lives, current level etc. into account.
	// Also calls the levelControl's UI methods to display info about the score calculation
	public virtual void CalculateUpdateShowScore(int totalMoves, float timeTaken, bool win, LevelControl levelControl) {
	}
}
