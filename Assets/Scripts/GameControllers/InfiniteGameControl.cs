﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InfiniteGameControl : GameControl
{
	System.Random rnd = new System.Random();

	float _robotSpeed;
	string _sceneName;
	string _currentMap;
	int _playerTiles;
	int _repairTiles;

	float _speedMultiplier = 1;
	int _lives = 3;

	int _totalScore = 0;
	int _levelsCompleted = 0;
	int _startCompleted = 0;

	int _scoreMultiplier;

	List<int> previousMapIndices;

	string[] maps = new string[] {
		"map1.tmx",
		"map2.tmx",
		"map3.tmx",
		"map4.tmx",
		"map5.tmx",
		"map4r1.tmx",
		"map4r2.tmx",
		"map4r3.tmx",
		"map4r4c.tmx"
	};

	public static void StartGame(string pScene="SinglePlayerDesigned", float startSpeed=1, int startLives=3, int startLevelsCompleted=0, int scoreMultiplier=1) {
		Debug.Log("Creating infinite game controller");
		currentGame = new InfiniteGameControl(pScene, startSpeed, startLives, startLevelsCompleted);
		currentGame.LoadNextMap();
	}

	public InfiniteGameControl(string pScene="SinglePlayerDesigned", float startSpeed=1, int startLives=3, int startLevelsCompleted=0, int scoreMultiplier=1) {
		_sceneName=pScene;
		_levelsCompleted=startLevelsCompleted;
		_startCompleted=startLevelsCompleted;
		_lives=startLives;
		_speedMultiplier=startSpeed;
		_scoreMultiplier=scoreMultiplier;

		_repairTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
			GameControl.CROSSING + GameControl.TCROSSING + GameControl.DEADEND;
		previousMapIndices=new List<int>();
		ChooseRandomLevelParameters();
	}

	void ChooseRandomLevelParameters() {
		if (previousMapIndices.Count>maps.Length/2) {
			previousMapIndices.RemoveAt(0);
		}

		int maxMap = _levelsCompleted<2?2:(_levelsCompleted<4?5:9);
		int mapIndex = rnd.Next(maxMap);
		int iterations = 0;
		while (previousMapIndices.Contains(mapIndex) && iterations<100) {
			mapIndex = rnd.Next(maxMap);
			iterations++;
		}
		previousMapIndices.Add(mapIndex);

		_currentMap = maps[mapIndex];

		_robotSpeed = (mapIndex<5) ? 0.6f : 0.25f;
		_robotSpeed *= _speedMultiplier;

		int maxModifier = _levelsCompleted<3?1:(_levelsCompleted<6?2:4);
		int modifier = rnd.Next(maxModifier);

		switch (modifier) {
			case 1:
				// V-split for player:
				_playerTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
					GameControl.CROSSING + GameControl.VSPLIT;
				_robotSpeed *= 0.85f;
				break;
			case 2:
				// only T-crossings + dead ends!:
				_playerTiles = GameControl.DEADEND + GameControl.TCROSSING;
				_robotSpeed *= 0.7f;
				break;
			case 3:
				// only corners, straight and crossing!:
				_playerTiles = GameControl.CORNER + GameControl.STRAIGHT + GameControl.CROSSING;
				_robotSpeed *= 0.7f;
				break;
			default:
				// default:
				_playerTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
					GameControl.CROSSING + GameControl.TCROSSING;
				break;
		}
		string levelInfo = "Next challenge:\tmap: "+_currentMap+"\tspeed: "+_robotSpeed+"\tmodifier: "+modifier+"\tcompleted: "+_levelsCompleted;
		Debug.Log(levelInfo);
		Logger.Instance.Write(levelInfo);
		_speedMultiplier *= 1.1f;
	}

	/*
		if (playerTiles>0) {
			_playerTiles=playerTiles;
		} else {
			_playerTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
				GameControl.CROSSING + GameControl.TCROSSING;
		}
		if (repairTiles>0) {
			_repairTiles=repairTiles;
		} else {
			_repairTiles = GameControl.EMPTY + GameControl.CORNER + GameControl.STRAIGHT + 
				GameControl.CROSSING + GameControl.TCROSSING + GameControl.DEADEND;
		}
	}*/

	public override string GetMap() {
		return _currentMap;
	}

	public override float GetSpeed() {
		return _robotSpeed;
	}

	// Returns the tiles that are available to the player as a bit mask:
	public override int GetAvailableTiles() {
		return _playerTiles;
	}

	// Returns the tiles that are available for grid repair as a bit mask:
	public override int GetRepairTiles() {
		return _repairTiles;
	}

	/////// Methods for changing state / updating progress:

	// Load the next level or go to the end screen / main menu:
	public override void LoadNextMap() {
		if (_lives>=0) {
			SceneManager.LoadSceneAsync(_sceneName);
		} else {
			currentGame=null;
			SceneManager.LoadSceneAsync("MainMenu");
		}
	}

	void Win(int player = 0) {
		_levelsCompleted++;
		//if (_lives<3) {
		//	_lives++;
		//}
		ChooseRandomLevelParameters();
	}

	void Lose() {
		_lives--;
	}

	public override void CalculateUpdateShowScore(int totalMoves, float timeTaken, bool win, LevelControl levelControl) {
		int levelScore = 0;

		if (win) {// TODO: fine tune per level!
			levelScore = // (int)(_scoreMultiplier * 10000000 / ((10+totalMoves) * timeTaken));
				_scoreMultiplier * Mathf.Max(1000 - (10 * totalMoves) - (int)(2 * timeTaken), 100);
		}
		_totalScore += levelScore;

		// Logging:
		string levelResult = "\twin: "+win+"\tmoves: "+totalMoves+"\ttime: "+timeTaken+"\tlevel score: "+levelScore+"\ttotal score: "+_totalScore;
		Logger.Instance.Write(levelResult);


		// UI feedback:
		int levelNum = SceneManager.GetActiveScene().buildIndex;
		if (win) {
			levelControl.ShowText("RE-PAIRED!");
			Win();
			levelControl.ShowMoveInfo("moves: "+totalMoves+" time: "+Mathf.RoundToInt(timeTaken));
			levelControl.ShowScoreInfo("level score: "+levelScore);
			levelControl.ShowHighScoreInfo("total score: "+_totalScore);
		} else {
			Lose();
			if (_lives<0) {
				if (HighScores.IsNewHighscore(_totalScore, levelNum)) {
					levelControl.ShowText("NEW HIGHSCORE!");
				} else {
					levelControl.ShowText("GAME OVER");
				}
				levelControl.ShowMoveInfo("completed: "+(_levelsCompleted-_startCompleted));
				levelControl.ShowScoreInfo("total score: "+_totalScore);
				levelControl.ShowHighScoreInfo("high score: "+HighScores.GetHighscore(levelNum));
			} else {
				levelControl.ShowText("TRY AGAIN!");
				levelControl.ShowMoveInfo("lives: "+_lives);
			}
		}

		if (_lives<0) {
			Logger.Instance.Close();
		}
	}

}
