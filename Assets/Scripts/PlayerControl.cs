﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using GamePadInput;

public class PlayerControl : MonoBehaviour {

    public float tileReplenishDelay;

    const int numKeySets = 3;
    [Tooltip("Bitmask: 1 = Player 1, 2 = Player 2, 4 = Player 1/Lefty")]
    public int selectionKeySet = 7;
    [Tooltip("Bitmask: 1 = Player 1, 2 = Player 2, 4 = Player 1/Lefty")]
    public int arrowKeySet = 7;

    public byte gamePadNumber = 0;

	public GameObject placementEffect;
    public GameObject tileTray;
    Transform selectableTilesPivot; // Where available tiles are placed

    Transform gridPivot;

	float arrowCooldownTimer;
	float selectionCooldownTimer;
	public float tickCooldown = 0.3f;

	public int MovesMade { get; private set; } = 0;

	private bool gridComplete; // When true, fire event and stop replenishing
    public Action<PlayerControl> onGridComplete;

	Button[] tileSelectionButtons = new Button[] {
		Button.X, Button.A, Button.B
	};

    // The keys for selecting one of the available tiles
    KeyCode[,] tileSelectionKeys = new KeyCode[,] {
		{KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3},
		{KeyCode.Alpha8, KeyCode.Alpha9, KeyCode.Alpha0},
		{KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6}
	};
	// the keys for moving the cursor
	KeyCode[,] arrowKeys = new KeyCode[,] {
		{KeyCode.T, KeyCode.H, KeyCode.G, KeyCode.F},
		{KeyCode.UpArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.LeftArrow},
		{KeyCode.W, KeyCode.D, KeyCode.S, KeyCode.A}
	};

    // The number of tiles the user can select
    public int numChoices;

    public Vector2Int Cursor { get { return cursor; } set { cursor=value; } }

    Vector2Int cursor;
    GridModel tileGrid;

    // Tiles which the user can select right now
    GameObject[] selectableTiles;

    // Tiles which the user currently cannot select (but are selectable). Together with selectableTiles,
    // it should contain all tiles in the GridModel's selectableTilePrefabs
    HashSet<GameObject> tempUnselectableTiles = new HashSet<GameObject>();

    public event Action<PlayerControl> onCursorMove;

	private void Awake() {
		cursor = Vector2Int.zero;
		if (Application.platform==RuntimePlatform.WebGLPlayer) {
			// Somehow, even the try/catch doesn't prevent fatal errors on WebGL:
			GamePad.DisableGamePadInput();
		} else {
			// For Mac vs Windows:
			GamePad.TestGamePadInput();
		}
	}

	void Start() {
        tileGrid = GridModel.main;
        selectableTilesPivot = tileTray.transform.Find("TilePivot");
        gridPivot = GameObject.FindGameObjectWithTag("TileGrid").transform;

        foreach (var tilePrefab in tileGrid.selectableTilePrefabs) {
            tempUnselectableTiles.Add(tilePrefab);
        }

        Debug.Assert(selectionKeySet > 0 && selectionKeySet < (0x1 << numKeySets));
        Debug.Assert(arrowKeySet > 0 && arrowKeySet < (0x1 << numKeySets));

        Debug.Assert(numChoices > 0 && numChoices <= tileGrid.selectableTilePrefabs.Length);
        Debug.Assert(numChoices <= tileSelectionKeys.GetLength(1));

        selectableTiles = new GameObject[numChoices];

        StartCoroutine(ReplenishSelectableTiles());
    }

    public GameObject SelectNewAvailableTile() {
        List<GameObject> shortList = new List<GameObject>();

        float maxSum = 0;
        foreach (var tilePrefab in tempUnselectableTiles) {
            if (tileGrid.CanPlaceTileSomewhere(tilePrefab)) {
                shortList.Add(tilePrefab);
                maxSum += tilePrefab.GetComponent<TileProbability>().probability;
            }
        }
        if (shortList.Count == 0) {
            foreach (var tile in selectableTiles) {
                if (tile != null && tileGrid.CanPlaceTileSomewhere(tile)) {
                    // No new tiles can (currently) be added to the available tiles,
                    // but some existing ones can still be placed
                    return null;
                }
            }

            Debug.Log("No new tiles can be added");
            gridComplete = true;
            if (onGridComplete != null) {
                onGridComplete(this);
            }

            return null;
        }

        float select = UnityEngine.Random.Range(0, maxSum);
        float sumSofar = 0;
        foreach (var tilePrefab in shortList) {
            sumSofar += tilePrefab.GetComponent<TileProbability>().probability;
            if (sumSofar >= select) {
                return tilePrefab;
            }
        }

        // This should never happen (not even in case of rounding artifacts...)
        Debug.Assert(false);
        return null;
    }

    void DumpTiles(string eventTitle) {
        var sb = new StringBuilder();
        sb.Append(eventTitle);
        sb.Append(", Selectable:");
        foreach (var tile in selectableTiles) {
            if (tile != null) {
                sb.Append(" ");
                sb.Append(tile.name);
            }
        }
        sb.Append(", Unselectable:");
        foreach (var tile in tempUnselectableTiles) {
            if (tile != null) {
                sb.Append(" ");
                sb.Append(tile.name);
            }
        }
        sb.Append("\n");
        Debug.Log(sb.ToString());
    }

    void ReplenishAvailableTile(int availableIndex) {
        var tilePrefab = SelectNewAvailableTile();
        if (tilePrefab == null) {
            return;
        }

        tempUnselectableTiles.Remove(tilePrefab);

        var tile = Instantiate(tilePrefab, selectableTilesPivot);
        tile.transform.position += new Vector3(availableIndex * 1.3f, 0, 0);
        ((PrefabSource)tile.AddComponent(typeof(PrefabSource))).prefabSource = tilePrefab;
        selectableTiles[availableIndex] = tile;

        // DumpTiles("Replenished tile");
    }

    IEnumerator ReplenishSelectableTiles() {
        while (!gridComplete) {
            yield return new WaitForSeconds(tileReplenishDelay);
            for (int i = 0; i < numChoices; i++) {
                if (selectableTiles[i] == null) {
                    ReplenishAvailableTile(i);
                    break;
                }
            }
        }
    }

    void PlaceTile(int availableIndex) {
		MovesMade++;

        var tile = selectableTiles[availableIndex];

        tileGrid.PlaceTile(tile, Cursor);
        tile.transform.SetParent(gridPivot);
        tile.transform.position = new Vector3(Cursor.x, 0, Cursor.y);
        tile.transform.rotation = Quaternion.identity;

        // Make existing tile at position available for replenishing
        selectableTiles[availableIndex] = null;
        tempUnselectableTiles.Add(tile.GetComponent<PrefabSource>().prefabSource);

        //DumpTiles("Placed tiles");
    }

    void CursorMoved() {
        if (onCursorMove != null) {
            onCursorMove(this);
        }

        arrowCooldownTimer = tickCooldown;
    }


    bool SelectionKeyPressed(int keyIndex) {
        for (int i = 0; i < numKeySets; i++) {
            if ((selectionKeySet & (0x1 << i)) != 0 && Input.GetKeyDown(tileSelectionKeys[i, keyIndex])) {
                return true;
            }
        }

        return false;
    }

    bool ArrowKeyPressed(int keyIndex) {
        for (int i = 0; i < numKeySets; i++) {
            if ((arrowKeySet & (0x1 << i)) != 0 && Input.GetKeyDown(arrowKeys[i, keyIndex])) {
                return true;
            }
        }

        return false;
    }

	void TryPlaceTile(int i) {
		if (selectableTiles[i] != null && tileGrid.CanPlaceTile(selectableTiles[i], Cursor)) {
			PlaceTile(i);
			selectionCooldownTimer=tickCooldown;
			if (placementEffect!=null) {
				GameObject fx = Instantiate(placementEffect, new Vector3(cursor.x, 0, cursor.y), Quaternion.identity);
			}
		} else {
			var audio = GetComponent<AudioSource>();
			if (audio!=null) {
				audio.Play();
			} else {
				Debug.Log("PlayerControl: trying to play audio; audio source missing");
			}
		}
	}

    void Update() {
		GamePadState state = GamePad.GetState(gamePadNumber);

		// Turbo:
		if (Input.GetKey(KeyCode.Return) || state.GetButton(Button.RightTrigger)) {
			LevelControl.turboPressed=true;
		}

		// Tile placement
		selectionCooldownTimer-=Time.deltaTime;
        for (int i = 0; i < numChoices; i++) {
            if (SelectionKeyPressed(i) || (state.GetButton(tileSelectionButtons[i]) && selectionCooldownTimer < 0)) {
				TryPlaceTile(i);
            }
        }

        // Cursor movement
		arrowCooldownTimer -= Time.deltaTime;
        if (ArrowKeyPressed(1) || (state.GetAxis(Axis.LeftStickX)>0 && arrowCooldownTimer<=0)) {
            cursor.x = (cursor.x + 1) % tileGrid.width;
            CursorMoved();
        }
        if (ArrowKeyPressed(3) || (state.GetAxis(Axis.LeftStickX)<0 && arrowCooldownTimer<=0)) {
            cursor.x = (cursor.x + tileGrid.width - 1) % tileGrid.width;
            CursorMoved();
        }
        if (ArrowKeyPressed(0) || (state.GetAxis(Axis.LeftStickY)>0 && arrowCooldownTimer<=0)) {
            cursor.y = (cursor.y + 1) % tileGrid.height;
            CursorMoved();
        }
        if (ArrowKeyPressed(2) || (state.GetAxis(Axis.LeftStickY)<0 && arrowCooldownTimer<=0)) {
            cursor.y = (cursor.y + tileGrid.height - 1) % tileGrid.height;
            CursorMoved();
        }

		// mouse control:
		if (Input.GetMouseButtonDown(gamePadNumber)) {
			RaycastHit hitInfo;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer("Raycast"))) {
				Debug.Log("Mouse click on raycast layer. Point: "+hitInfo.point);
				if (hitInfo.collider.tag=="RaycastFloor") {
					cursor.x=Mathf.RoundToInt(hitInfo.point.x);
					cursor.y=Mathf.RoundToInt(hitInfo.point.z);
					CursorMoved();
				} else { // it must be the tray
					Debug.Log("Not floor. Uv: "+hitInfo.textureCoord);
					TryPlaceTile((int)(hitInfo.textureCoord.x*3));
				}
			} else {
				Debug.Log("Mouse click missed");
			}
		}
    }
}
