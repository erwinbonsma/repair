﻿using System;
using System.Runtime.InteropServices;   // For DllImport
using UnityEngine;

namespace GamePadInput
{
	public enum Button {
		DPadUp = 0,
		DPadDown = 1,
		DPadLeft = 2,
		DPadRight = 3,
		Start = 4,
		Back = 5,
		LeftThumb = 6,
		RightThumb = 7,
		LeftShoulder = 8,
		RightShoulder = 9,
		Guide = 10,
		None = 11,
		A = 12,
		B = 13,
		X = 14,
		Y = 15,
		LeftTrigger = 16,
		RightTrigger = 17	}

	public enum Axis {
		LeftStickX = 0,
		LeftStickY = 1,
		RightStickX = 2,
		RightStickY = 3,
		LeftTriggerAxis = 4,
		RightTriggerAxis = 5,
		ThumbPadX = 6,
		ThumbPadY = 7
	}

	public enum GamePadDeadZone {
		Circular,
		IndependentAxes,
		None	}

    public struct GamePadState
    {
        [StructLayout(LayoutKind.Sequential)]
        internal struct RawState
        {
            public uint dwPacketNumber;
            public ushort wButtons;
            public byte bLeftTrigger;
            public byte bRightTrigger;
            public short sThumbLX;
            public short sThumbLY;
            public short sThumbRX;
            public short sThumbRY;
        }

        bool isConnected;
        uint packetNumber;
		RawState rawState;
		float leftThumbX,leftThumbY,rightThumbX,rightThumbY;
		float leftTrigger, rightTrigger;

		public uint PacketNumber {
			get { return packetNumber; }
		}

		public bool IsConnected {
			get { return isConnected; }
		}

		public bool GetButton(Button butt) {
			if ((int)butt >= 16) // trigger
				return GetAxis((Axis)((int)butt - 12)) >= GamePad.TriggerCutoff; // yeah this is a bit ugly
			return (rawState.wButtons & (1<<(int)butt)) != 0;
		}

		public float GetAxis(Axis ax) {
			switch (ax) {
				case Axis.LeftStickX: return leftThumbX;
				case Axis.LeftStickY: return leftThumbY;
				case Axis.RightStickX: return rightThumbX;
				case Axis.RightStickY: return rightThumbY;
				case Axis.LeftTriggerAxis: return leftTrigger;
				case Axis.RightTriggerAxis: return rightTrigger;
				case Axis.ThumbPadX: return (GetButton(Button.DPadLeft) ? -1 : 0) + (GetButton(Button.DPadRight) ? 1 : 0);
				case Axis.ThumbPadY: return (GetButton(Button.DPadDown) ? -1 : 0) + (GetButton(Button.DPadUp) ? 1 : 0);
				default: return 0;
			}
		}

        internal GamePadState(bool isConnected, RawState rawState, GamePadDeadZone deadZone, float deadZoneRange = 0.35f)
        {
            this.isConnected = isConnected;

            if (!isConnected)
            {
                rawState.dwPacketNumber = 0;
                rawState.wButtons = 0;
                rawState.bLeftTrigger = 0;
                rawState.bRightTrigger = 0;
                rawState.sThumbLX = 0;
                rawState.sThumbLY = 0;
                rawState.sThumbRX = 0;
                rawState.sThumbRY = 0;
            }
            this.rawState = rawState;

            packetNumber = rawState.dwPacketNumber;
			leftThumbX = 1f * rawState.sThumbLX / short.MaxValue;
			leftThumbY = 1f * rawState.sThumbLY / short.MaxValue;
			GamePadUtil.ApplyDeadZoneVector2(ref leftThumbX, ref leftThumbY, deadZone, deadZoneRange);
			rightThumbX = 1f * rawState.sThumbRX / short.MaxValue;
			rightThumbY = 1f * rawState.sThumbRY / short.MaxValue;
            GamePadUtil.ApplyDeadZoneVector2(ref rightThumbX, ref rightThumbY, deadZone, deadZoneRange);
			leftTrigger = GamePadUtil.ApplyDeadZoneFloat(1f * rawState.bLeftTrigger / byte.MaxValue, deadZoneRange);
			rightTrigger = GamePadUtil.ApplyDeadZoneFloat(1f * rawState.bRightTrigger / byte.MaxValue, deadZoneRange);
        }

    }

	public static class GamePadUtil {
		public static void ApplyDeadZoneVector2(ref float x, ref float y, GamePadDeadZone deadZoneType, float deadZoneRange) {
			switch (deadZoneType) {
				case GamePadDeadZone.IndependentAxes:
					x = ApplyDeadZoneFloat(x, deadZoneRange);
					y = ApplyDeadZoneFloat(y, deadZoneRange);
					break;
				case GamePadDeadZone.Circular:
					float len = (float)Math.Sqrt(x * x + y * y);
					float coefficient = ApplyDeadZoneFloat(len, deadZoneRange);
					if (coefficient <= 0) {
						x = 0;
						y = 0;
					}
					else {
						x *= coefficient / len;
						y *= coefficient / len;
					}
					break;
					// default or DeadZone.None: no changes.
			}
		}

		// Assumptions: deadZoneRange is positive, x is ideally between -maxVal and maxVal, maxVal>deadZoneRange.
		// Output: a value between -maxVal and maxVal, 0 if |x|<=deadZoneRange; a continuous piecewise linear function of x.
		public static float ApplyDeadZoneFloat(float x, float deadZoneRange, float maxVal = 1) {
			if (x > maxVal)
				return maxVal;
			else if (x < -maxVal)
				return -maxVal;
			else if (x < -deadZoneRange)
				x += deadZoneRange;
			else if (x > deadZoneRange)
				x -= deadZoneRange;
			else
				return 0;
			return x / (maxVal - deadZoneRange);
		}
	}

    public static class GamePad
    {
		public static float TriggerCutoff = 0.2f;
		public static GamePadDeadZone DefaultDeadZone = GamePadDeadZone.IndependentAxes;
		public const uint Success = 0x000;
		//public const uint NotConnected = 0x000;
		static bool DllFound = true;


        public static GamePadState GetState(byte playerIndex)
        {
			return GetState(playerIndex, DefaultDeadZone);
        }

        public static GamePadState GetState(byte playerIndex, GamePadDeadZone deadZone)
        {
			if (DllFound) {
				GamePadState.RawState state;
				uint result = Imports.XInputGamePadGetState(playerIndex, out state);
				return new GamePadState(result == Success, state, deadZone);
			} else {
				return new GamePadState(false, new GamePadState.RawState(), DefaultDeadZone);
			}
        }

        public static void SetVibration(byte playerIndex, float leftMotor, float rightMotor)
        {
            Imports.XInputGamePadSetState(playerIndex, leftMotor, rightMotor);
        }

		public static void DisableGamePadInput() {
			DllFound=false;
		}

		public static void TestGamePadInput() {
			try {
				GetState(0);
				//Debug.Log ("Dll found: Game pad input can be retrieved");
			} catch (Exception error){
				Debug.Log ("Error while retrieving game pad state: "+error.Message);
				DllFound = false;
			}
		}
    }

	class Imports {
		internal const string DLLName = "XInputInterface";

		[DllImport(DLLName)]
		public static extern uint XInputGamePadGetState(uint playerIndex, out GamePadState.RawState state);
		[DllImport(DLLName)]
		public static extern void XInputGamePadSetState(uint playerIndex, float leftMotor, float rightMotor);
	}
}
