﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamePadInput;

public class InputTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		GamePadState state = GamePad.GetState(0);
		Debug.Log(state.GetAxis(Axis.LeftStickX));
    }
}
