﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectorAdvanced : Director {
	[Tooltip("When coming from north: 1=can turn left, 2=can go straight, 4=can turn right.")]
	public int NorthTurns;
	[Tooltip("When coming from east: 1=can turn left, 2=can go straight, 4=can turn right.")]
	public int EastTurns;
	[Tooltip("When coming from south: 1=can turn left, 2=can go straight, 4=can turn right.")]
	public int SouthTurns;
	[Tooltip("When coming from west: 1=can turn left, 2=can go straight, 4=can turn right.")]
	public int WestTurns;

	public override bool HardTurns { get { return false; } }

	public override Vector2Int GetNewDirection(Vector2Int inDirection) {
		Connections conn = GetComponent<Connections>();

		int possibleTurns = 0;
		if (inDirection.y > 0) { // coming from South
			possibleTurns = SouthTurns;
		} else if (inDirection.x > 0) { // coming from West
			possibleTurns = WestTurns;
		} else if (inDirection.y < 0) { // coming from North
			possibleTurns = NorthTurns;
		} else if (inDirection.x < 0) { // coming from East
			possibleTurns = EastTurns;
		} 

		if ((possibleTurns & 2)>0) {// can go straight
			return inDirection;
		}
		// Debug.Log("Checking turns using possibleTurns = "+possibleTurns);
		Vector2Int turnLeft = new Vector2Int(-inDirection.y, inDirection.x);
		Vector2Int turnRight = turnLeft * -1;

		if (possibleTurns==5) { // both left and right possible
			// choose randomly.
			if (Random.value<0.5f) {
				return turnLeft;
			} else {
				return turnRight;
			}
		}
		if (possibleTurns==1) { // only turn left
			return turnLeft;
		}
		if (possibleTurns==4) { // only turn right
			return turnRight;
		}
		// nowhere to go... possibleTurns must be 0.
		return inDirection * -1;
	}
}

