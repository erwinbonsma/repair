﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(Connections))]
public class Director : MonoBehaviour {
	public bool Selected;

	// Returns "true" if the turns are orthogonal, "false" if they are diagonal.
	public virtual bool HardTurns { get { return true; } }

	public virtual Vector2Int GetNewDirection(Vector2Int inDirection, out bool immediate) {
		Connections conn = GetComponent<Connections>();

		if (!conn.InDirectionPossible(inDirection)) {
			immediate=true;
			return inDirection * -1; // turn around!
		}
		immediate=false; // we can go in
		return GetNewDirection(inDirection);
	}

	public virtual Vector2Int GetNewDirection(Vector2Int inDirection) {
		Connections conn = GetComponent<Connections>();

		if (conn.OutDirectionPossible(inDirection)) {
			return inDirection; // continue straight
		}
		// Debug.Log("Checking turns");
		Vector2Int turnLeft = new Vector2Int(-inDirection.y, inDirection.x);
		Vector2Int turnRight = turnLeft * -1;

		if (conn.OutDirectionPossible(turnLeft) && conn.OutDirectionPossible(turnRight)) {
			// choose randomly.
			if (Random.value<0.5f) {
				return turnLeft;
			} else {
				return turnRight;
			}
		}
		if (conn.OutDirectionPossible(turnLeft)) {
			return turnLeft;
		}
		if (conn.OutDirectionPossible(turnRight)) {
			return turnRight;
		}
		// nowhere to go...
		return inDirection * -1;
	}

	// test code
	public void Update() {
		if (!Selected)
			return;

		bool immediate;
		Vector2Int inDir=Vector2Int.zero;
		if (Input.GetKeyDown(KeyCode.W)) {
			inDir=new Vector2Int(0, 1);  // north
		}
		if (Input.GetKeyDown(KeyCode.S)) {
			inDir=new Vector2Int(0, -1);  // south
		}
		if (Input.GetKeyDown(KeyCode.A)) {
			inDir=new Vector2Int(-1, 0);  // east
		}
		if (Input.GetKeyDown(KeyCode.D)) {
			inDir=new Vector2Int(1, 0);  // west
		}
		if (inDir.x==0 && inDir.y==0)
			return;

		Debug.Log("In direction: "+inDir);
		Connections conn = GetComponent<Connections>();

		Debug.Log(" in possible: "+conn.InDirectionPossible(inDir));
		Debug.Log(" out possible: "+conn.OutDirectionPossible(inDir));
		Debug.Log(" New direction: "+GetNewDirection(inDir, out immediate));
		Debug.Log(" immediate: "+immediate);
	}
}
