﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Connections : MonoBehaviour {
	public bool North;
	public bool East;
	public bool South;
	public bool West;

	public bool IsEmpty { get { return !(North || East || South || West); } }

	bool autoRegister = true;
	public void SkipRegister() {
		autoRegister=false;
	}

	public int Code {
		get {
			return
				(North ? 1 : 0) +
				(East  ? 2 : 0) +
				(South ? 4 : 0) +
				(West  ? 8 : 0);
		}
	}

	private void Start() {
		if (!GridModel.main.Awoken && autoRegister) {
			Vector2Int pos = new Vector2Int(
				Mathf.RoundToInt(transform.position.x), 
				Mathf.RoundToInt(transform.position.z)
			);
			// Debug.Log("Tile placement during Start, at "+pos.x+","+pos.y);
			GridModel.main.ForcePlaceTile(gameObject, pos);
		}
	}

	// maps y to z direction in grid, so (0,1) is north, (1,0) is east.
	public bool InDirectionPossible(Vector2Int inDir) {
		if (inDir.x * inDir.y!=0) {
			throw new Exception("This method only works for grid aligned vectors");
		}
		if (inDir.x>0) {
			return West;
		} else if (inDir.x<0) {
			return East;
		} else if (inDir.y>0) {
			return South;
		} else if (inDir.y<0) {
			return North;
		}
		return false; // vector must be (0,0). That's bogus!
	}
		
	// maps y to z direction in grid, so (0,1) is north, (1,0) is east.
	public bool OutDirectionPossible(Vector2Int outDir) {
		if (outDir.x * outDir.y!=0) {
			throw new Exception("This method only works for grid aligned vectors");
		}
		if (outDir.x>0) {
			return East;
		} else if (outDir.x<0) {
			return West;
		} else if (outDir.y>0) {
			return North;
		} else if (outDir.y<0) {
			return South;
		}
		return false; // vector must be (0,0). That's bogus!
	}
}
