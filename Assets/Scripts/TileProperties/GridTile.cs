﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTile {
    public GameObject Prefab { get; private set; }
    public float Probability { get; private set; }

    public GridTile(GameObject prefab, float prob) {
        Prefab = prefab;
        Probability = prob;
    }
}
