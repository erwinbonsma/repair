using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace TiledMapParser
{
	public class MapParser {
		static XmlSerializer serial = new XmlSerializer (typeof(Map));
		static XmlSerializer tileSetSerializer = new XmlSerializer (typeof (TileSet));

		public static Map ReadMap(string filename) {
			return ReadMap(new StreamReader(filename));
		}

		public static Map ReadMap(TextReader filereader) {
			XmlSerializer serial = new XmlSerializer(typeof(Map));
			Map map = (Map)serial.Deserialize (filereader);
			map.InitializeObjects();
			return map;
		}

		public static TileSet ReadTileSet(string filename) {
			TextReader reader = new StreamReader (filename);
			TileSet tileSet;
			try 
			{
				tileSet = (TileSet)tileSetSerializer.Deserialize (reader);
			} catch (Exception error) {
				StringBuilder builder = new StringBuilder ("TiledMapParser.cs: Error while reading XML file for tileSet ");
				builder.Append (filename);
				builder.AppendFormat (". Error message: {0}", error.Message);
				throw new Exception (builder.ToString ());
			} finally {
				reader.Close ();
			}

			return tileSet;
		}

		public static void WriteMap(string filename, Map map) {
			TextWriter writer = new StreamWriter (filename);
			serial.Serialize (writer, map);
			writer.Close ();
		}
	}

	[XmlRootAttribute("map")]
	public class Map : PropertyContainer
	{
		[XmlAttribute("width")]
		public int Width;
		[XmlAttribute("height")]
		public int Height;

		[XmlAttribute("version")]
		public string Version;
		[XmlAttribute("orientation")]
		public string Orientation;
		[XmlAttribute("renderorder")]
		public string RenderOrder;
		[XmlAttribute("tilewidth")]
		public int TileWidth;
		[XmlAttribute("tileheight")]
		public int TileHeight;
		[XmlAttribute("nextobjectid")]
		public int NextObjectId;

		[XmlElement("tileset")]
		public TileSet[] TileSets;

		[XmlElement("layer")]
		public Layer[] Layers;

		[XmlElement("objectgroup")]
		public ObjectGroup[] ObjectGroups;

		[XmlElement("imagelayer")]
		public ImageLayer[] ImageLayers;

		[XmlText]
		public string InnerXML;	// This should be empty

		override public string ToString() {
			string output = "Map of width " + Width + " and height " + Height + ".\n";

			output += "TILE LAYERS:\n";
			foreach (Layer l in Layers)
				output += l.ToString ();

			output += "IMAGE LAYERS:\n";
			foreach (ImageLayer l in ImageLayers)
				output += l.ToString ();

			output += "TILE SETS:\n";
			foreach (TileSet t in TileSets)
				output += t.ToString ();

			output += "OBJECT GROUPS:\n";
			foreach (ObjectGroup g in ObjectGroups)
				output += g.ToString ();

			return output;
		}

		// (new)
		// A helper function that returns the tile set that belongs to the tile ID read from the layer data:
		public TileSet GetTileSet(int tileID) {
			if (tileID < 0) // || TileSets==null)
				return null;
			int index = 0;
			while (TileSets[index].FirstGId + TileSets[index].TileCount <= tileID) {
				index++;
				if (index >= TileSets.Length)
					return null;
			}
			return TileSets [index];
		}

		/// <summary>
		/// Clean up the name of all properties, i.e., remove spaces and change all characters to lower case.
		/// </summary>
		public override void CleanUp() 
		{
			if(Layers != null) {
				foreach (Layer layer in Layers) {
					layer.CleanUp ();
				}
			}
			if(ImageLayers != null) {
				foreach (ImageLayer layer in ImageLayers) {
					layer.CleanUp ();
				}
			}
			if(ObjectGroups != null) {
				foreach (ObjectGroup group in ObjectGroups) {
					group.CleanUp ();
					if (group.Objects != null) {
						foreach (TiledObject obj in group.Objects) {
							obj.CleanUp ();
						}
					}
				}
			}
			// These should now stay case sensitive:
			// base.CleanUp ();
		}

		public void InitializeObjects() {
			if(ObjectGroups != null) {
				foreach (ObjectGroup group in ObjectGroups) {
					if (group.Objects != null) {
						foreach (TiledObject obj in group.Objects) {
							obj.Initialize ();
						}
					}
				}
			}
		}

		HashSet<TileSet> tileSetUsed=null;

		public bool IsUsed(TileSet tiles) {
			if (tileSetUsed == null) {
				FindUsedTileSets ();
			}
			return (tileSetUsed.Contains(tiles));
		}

		void FindUsedTileSets() {
			Console.WriteLine ("TiledMap: Finding all tile sets that are used in tile layers");
			tileSetUsed = new HashSet<TileSet> ();
			if (Layers == null)
				return;
			foreach (Layer layer in Layers) {
				string[] chars = layer.Data.innerXML.Split (',');
				foreach (string tileNum in chars) {
					int tileID=(int)(uint.Parse(tileNum) & 0x1FFFFFFF);
					if (tileID!=0) {
						tileSetUsed.Add(GetTileSet(tileID));
					}
				}
			}
		}
	}

	// !!!!!!!!!!!!!!!!!!
	[XmlRootAttribute("tileset")]
	public class TileSet {
		[XmlAttribute("tilewidth")]
		public int TileWidth;
		[XmlAttribute("tileheight")]
		public int TileHeight;
		[XmlAttribute("tilecount")]
		public int TileCount;
		[XmlAttribute("columns")]
		public int Columns;

		public int Rows {
			get {
				if (TileCount % Columns == 0)
					return TileCount / Columns;
				else
					return (TileCount / Columns) + 1;
			}
		}

		// This is the number of the first tile. Usually 1 (so 0 = empty).
		// When multiple tilesets are used, this is the total number of previous tiles + 1.
		[XmlAttribute("firstgid")]
		public int FirstGId;
		[XmlAttribute ("source")]
		public string source;

		//version="1.2" tiledversion="1.2.0"
		[XmlAttribute ("version")]
		public string Version;
		[XmlElement ("tiledversion")]
		public string TiledVersion;

		[XmlAttribute ("name")]
		public string Name;
		[XmlElement("image")]
		public Image Image;

		override public string ToString() {
			return "Tile set: Name: " + Name + " Image: " + Image + " Tile dimensions: " + TileWidth + "x" + TileHeight + " Grid dimensions: " + Columns + "x" + (int)Math.Ceiling (1f * TileCount / Columns)+"\n";
		}

		/// <summary>
		/// Copy the property values from a loaded TileSet, but keeps the FirstGId value by default.
		/// It doesn't copy the source, since originally this TileSet should have a source that was used to load the loadedTileSet variable.
		/// </summary>
		/// <param name="loadedTileSet">Loaded TileSet.</param>
		public void CopyFromLoadedTileSet(TileSet loadedTileSet, bool keepFirstGId = true) 
		{
			this.TileWidth = loadedTileSet.TileWidth;
			this.TileHeight = loadedTileSet.TileHeight;
			this.TileCount = loadedTileSet.TileCount;
			this.Columns = loadedTileSet.Columns;
			this.Version = loadedTileSet.Version;
			this.TiledVersion = loadedTileSet.TiledVersion;
			this.Name = loadedTileSet.Name; 
			this.Image = loadedTileSet.Image;

			if(!keepFirstGId) 
			{
				this.FirstGId = loadedTileSet.FirstGId;
			}
		}
	}

	public class PropertyContainer {
		[XmlElement("properties")]
		public PropertyList propertyList;

		public bool HasProperty(string key, string type) {
			if (propertyList==null)
				return false;
			foreach (Property p in propertyList.properties) {
				if (p.Name == key && p.Type==type)
					return true;
			}
			return false;
		}

		public string GetStringProperty(string key, string defaultValue="") {
			if (propertyList==null)
				return defaultValue;
			foreach (Property p in propertyList.properties) {
				if (p.Name == key) // && p.Type=="")
					return p.Value;
			}
			return defaultValue;
		}

		public float GetFloatProperty(string key, float defaultValue=1) {
			if (propertyList==null)
				return defaultValue;
			foreach (Property p in propertyList.properties) {
				if (p.Name == key && p.Type=="float")
					return float.Parse(p.Value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
			}
			return defaultValue;
		}

		public int GetIntProperty(string key, int defaultValue=1) {
			if (propertyList==null)
				return defaultValue;
			foreach (Property p in propertyList.properties) {
				if (p.Name == key && p.Type=="int")
					return int.Parse(p.Value);
			}
			return defaultValue;
		}

		public bool GetBoolProperty(string key, bool defaultValue=false) {
			if (propertyList==null)
				return defaultValue;
			foreach (Property p in propertyList.properties) {
				if (p.Name == key && p.Type=="bool")
					return bool.Parse(p.Value);
			}
			return defaultValue;
		}

		public uint GetColorProperty(string key, uint defaultvalue=0xffffffff) {
			if (propertyList==null)
				return defaultvalue;
			foreach (Property p in propertyList.properties) {
				if (p.Name == key && p.Type == "color") {
					return Utils.GetColor (p.Value);
					/*
					return (uint)(
						(Convert.ToInt32(p.Value.Substring(3, 2), 16)<<24) + 
						(Convert.ToInt32(p.Value.Substring(5, 2), 16)<<16) +
						(Convert.ToInt32(p.Value.Substring(7, 2), 16)<<8) +
						(Convert.ToInt32(p.Value.Substring(1, 2), 16)));
						*/
				}
			}
			return defaultvalue;
		}

		public virtual void CleanUp() {
			if (propertyList != null) {
				foreach (Property property in propertyList.properties) {
					property.Name = property.Name.Trim ().Replace (" ", "").ToLower ();
				}
			}
		}
	}

	[XmlRootAttribute("imagelayer")]
	public class ImageLayer : PropertyContainer {
		[XmlAttribute("name")]
		public string Name;
		[XmlElement("image")]
		public Image Image;
		[XmlAttribute("offsetx")]
		public float offsetX = 0;
		[XmlAttribute("offsety")]
		public float offsetY = 0;

		override public string ToString() {
			return "Image layer: " + Name + " Image: " + Image + "\n";
		}
	}

	[XmlRootAttribute("image")]
	public class Image {
		[XmlAttribute("width")]		// width in pixels
		public int Width;
		[XmlAttribute("height")]	// height in pixels
		public int Height;
		[XmlAttribute("source")]	// AnimSprite file name
		public string FileName;

		override public string ToString() {
			return FileName + " (dim: "+Width+"x"+Height+")";
		}
	}

	[XmlRootAttribute("layer")]
	public class Layer : PropertyContainer {
		[XmlAttribute("name")]
		public string Name;
		[XmlAttribute("width")]
		public int Width;
		[XmlAttribute("height")]
		public int Height;
		[XmlElement("data")]
		public Data Data;

		override public string ToString() {
			string output = " Layer name: " + Name;
			output += "Properties:\n"+propertyList.ToString();
			output += "Data:" + Data.ToString ();
			return output;
		}

		
		/// <summary>
		/// Returns the tile data from this layer as a 2-dimensional array of shorts. 
		/// It's a column-major array, so use [column,row] as indices.
		/// 
		/// This method does a lot of string parsing and memory allocation, so use it only once,
		/// during level loading.
		/// </summary>
		/// <returns>The tile array.</returns>
		public short[,] GetTileArray() {
			short[,] grid = new short[Width, Height];
			string[] lines = Data.innerXML.Split ('\n');
			int row = 0;

			foreach (string line in lines) {
				if (line.Length <= 1)
					continue;
				string parseLine = line;
				if (line [line.Length - 1] == ',')
					parseLine = line.Substring (0, line.Length - 1);

				string[] chars = parseLine.Split (',');
				for (int col = 0; col < chars.Length; col++) {
					if (col < Width) {
						short tileNum = short.Parse (chars [col]);
						grid [col, row] = tileNum;
					}
				}
				row++;
			}

			return grid;
		}
	}

	[XmlRootAttribute("data")]
	public class Data {
		[XmlAttribute("encoding")]
		public string Encoding;
		[XmlText]
		public string innerXML;

		override public string ToString() {
			return innerXML;
		}
	}

	[XmlRootAttribute("properties")]
	public class PropertyList {
		[XmlElement("property")]
		public Property[] properties;

		override public string ToString() {
			string output = "";
			foreach (Property p in properties)
				output += p.ToString ();
			return output;
		}
	}

	[XmlRootAttribute("property")]
	public class Property {
		[XmlAttribute("name")]
		public string Name;
		[XmlAttribute("type")]
		public string Type="string";
		[XmlAttribute("value")]
		public string Value;

		public Property() {
		}

		public Property(string pName, string pType, string pValue) {
			Name = pName;
			Type = pType;
			Value = pValue;
		}

		override public string ToString() {
			return "Property: Name: " + Name + " Type: " + Type + " Value: " + Value + "\n";
		}
	}

	[XmlRootAttribute("objectgroup")]
	public class ObjectGroup : PropertyContainer {
		[XmlAttribute("name")]
		public string Name;
		[XmlElement("object")]
		public TiledObject[] Objects;

		override public string ToString() {
			string output = "Object group: Name: " + Name + " Objects:\n";
			foreach (TiledObject obj in Objects)
				output += obj.ToString ();

			return output;
		}
	}

	[XmlRootAttribute("text")]
	public class Text {
		[XmlAttribute("fontfamily")]
		public string font;
		[XmlAttribute("wrap")]
		public int wrap=0;
		[XmlAttribute("bold")]
		public int bold=0;
		[XmlAttribute("italic")]
		public int italic=0;
		[XmlAttribute("pixelsize")]
		public int fontSize = 16;
		[XmlText]
		public string text;
		[XmlAttribute("halign")]
		public string horizontalAlign="left";
		[XmlAttribute("valign")]
		public string verticalAlign="top";
		[XmlAttribute("color")]
		public string color="#FF000000"; // Tiled default

		public uint Color {
			get {
				//if (color != null) {
					return Utils.GetColor (color);
				//} else {
				//	return 0xFFFF0000; // Tiled default
				//}
			}
		}

		override public string ToString() {
			return text;
		}
	}

	[XmlRootAttribute("object")]
	public class TiledObject : PropertyContainer {
		[XmlAttribute("id")]
		public int ID;


		[XmlAttribute("gid")]
		public uint GID=0xffffffff;
		// Tiled's GID (with two flip bits) is processed into these three fields, after calling Initialize:
		public int ImageID=-1;
		public bool MirrorX = false;
		public bool MirrorY = false;

		[XmlAttribute("rotation")]
		public float Rotation = 0;
		[XmlAttribute("name")]
		public string Name;
		[XmlAttribute("type")]
		public string Type;
		[XmlAttribute("width")]		// width in pixels
		public float Width;
		[XmlAttribute("height")]	// height in pixels
		public float Height;
		[XmlAttribute("x")]
		public float X;
		[XmlAttribute("y")]
		public float Y;
		[XmlElement("text")]
		public Text textField;

		public void Initialize() {
			if (GID != 0xffffffff) {
				ImageID = (int)(GID & 0x3fffffff);
				MirrorX = (GID & 0x80000000) > 0;
				MirrorY = (GID & 0x40000000) > 0;
			}
		}

		override public string ToString() {
			return "Object: " + Name + " ID: " + ID + " Type: " + Type + " coordinates: (" + X + "," + Y + ") dimensions: (" + Width + "," + Height + ")\n";	
		}
	}

	public class Utils {
		public static uint GetColor(string htmlColor) {
			if (htmlColor.Length == 9) {
				return (uint)(
				    (Convert.ToInt32 (htmlColor.Substring (3, 2), 16) << 24) +		// R
				    (Convert.ToInt32 (htmlColor.Substring (5, 2), 16) << 16) +		// G
				    (Convert.ToInt32 (htmlColor.Substring (7, 2), 16) << 8) +		// B
				    (Convert.ToInt32 (htmlColor.Substring (1, 2), 16)));			// Alpha
			} else if (htmlColor.Length == 7) {
				return (uint)(
				    (Convert.ToInt32 (htmlColor.Substring (1, 2), 16) << 24) +		// R
				    (Convert.ToInt32 (htmlColor.Substring (3, 2), 16) << 16) +		// G
				    (Convert.ToInt32 (htmlColor.Substring (5, 2), 16) << 8) +		// B
					0xFF);															// Alpha
			} else {
				throw new Exception ("Cannot recognize color string: " + htmlColor);
			}
		}
	}
}
