﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TiledMapParser;
using System.IO;
using UnityEngine.Networking;

public class TiledMapLoader : MonoBehaviour
{
	[Tooltip("Use this to test a specific map. Set to -1 for random map selection.")]
	public int FixedChoice = -1;
	public string[] filenames;
	public float robotSpeed = 1;
	public GameObject[] prefabs;
	public GameObject[] robots;

	void Start() {
		var gameControl = GameControl.currentGame;
		string filename=null;
		if (gameControl!=null) {
			filename=gameControl.GetMap();
			robotSpeed=gameControl.GetSpeed();
			if (filename!=null) {
				//Debug.Log("Getting tiled map & robot speed from game controller: "+filename+", "+robotSpeed);
			} else {
				//Debug.Log("Getting robot speed from game controller: "+robotSpeed);
			}
			UpdateAvailableTiles(gameControl);
		}
		if (filename==null) {
			int selected = FixedChoice;
			while (selected<0 || selected>=filenames.Length) {
				selected = (int)(Random.value*filenames.Length);
			}
			filename = filenames[selected];
		}

		string extendedFilename = Path.Combine( Application.streamingAssetsPath, filename);

		if (Application.platform==RuntimePlatform.WebGLPlayer) {
			//Debug.Log("WebGL loading for path "+extendedFilename);
			StartCoroutine(LoadMapFromRemote(extendedFilename));
			//return MapParser.ReadMap(UnityWebRequest.Get(extendedFilename));
		} else {
			//Debug.Log("Loading Tiled map: "+extendedFilename);
			Map input = MapParser.ReadMap(new StreamReader(extendedFilename));
			SpawnTiles(input);
			SpawnRobots(input);
		}
    }

	void UpdateAvailableTiles(GameControl gameControl) {
		GridModel gridModel = GridModel.main;
		int playerTiles = gameControl.GetAvailableTiles();
		int repairTiles = gameControl.GetRepairTiles();

		List<GameObject> playerTilePrefabs=new List<GameObject>();
		List<GameObject> repairTilePrefabs=new List<GameObject>();
		for (int i=0;i<prefabs.Length;i++) {
			if ((playerTiles & (1<<i))>0) {
				playerTilePrefabs.Add(prefabs[i]);
			}
			if ((repairTiles & (1<<i))>0) {
				repairTilePrefabs.Add(prefabs[i]);
			}
		}
		//Debug.Log("Overriding the available tile lists from grid model");
		gridModel.selectableTilePrefabs = playerTilePrefabs.ToArray();
		gridModel.tileRepairPrefabs = repairTilePrefabs.ToArray();
	}

	IEnumerator LoadMapFromRemote(string filename) {
		Debug.Log("Loading map data from remote: "+filename);
		var webRequest = UnityWebRequest.Get(filename);
		yield return webRequest.SendWebRequest();

		if (!webRequest.isNetworkError && !webRequest.isHttpError) {
			var textReader = new StringReader(webRequest.downloadHandler.text);
			Map input = MapParser.ReadMap(textReader);
			SpawnTiles(input);
			SpawnRobots(input);
		} else {
			Debug.Log("Network / http error when loading map");
		}
	}

	void SpawnTiles(Map input) {
		GridModel grid=GridModel.main;
		var tileGrid = GameObject.FindGameObjectWithTag("TileGrid").transform;

		short[,] tiledata = input.Layers[0].GetTileArray();
		for (int col=0;col<input.Width;col++) {
			for (int row=0;row<input.Height;row++) {
				if (tiledata[col, row]>0) {
					var tile = Instantiate(prefabs[tiledata[col, row]-1], tileGrid);
					tile.GetComponent<Connections>().SkipRegister();
					if (grid is ModificationGrid) {
						((ModificationGrid)grid).SimpleForcePlaceTile(tile, new Vector2Int(col, input.Height-1-row), true);
					} else {
						grid.ForcePlaceTile(tile, new Vector2Int(col, input.Height-1-row), true);
					}
				}
			}
		}
	}

	void SpawnRobots(Map input) {
		if (input.ObjectGroups!=null && input.ObjectGroups.Length>0) {
			//Debug.Log("Tiled map contains robot positions");
			foreach (TiledObject obj in input.ObjectGroups[0].Objects) {
				//Debug.Log("Spawning object?");
				if (obj.GID>0) {
					TileSet tiles = input.GetTileSet(obj.ImageID);
					int typeIndex = obj.ImageID - tiles.FirstGId;
					//Debug.Log("type: "+typeIndex);

					Vector2 centerOffset = new Vector2(obj.Width/2, -obj.Height/2);
					float angle = Mathf.PI * obj.Rotation / 180;
					float cos = Mathf.Cos(angle);
					float sin = Mathf.Sin(angle);
					// rotate center offset, just like Tiled does:
					centerOffset = new Vector2(
						cos * centerOffset.x - sin * centerOffset.y,
						sin * centerOffset.x + cos * centerOffset.y
					);

					int col = (int)((obj.X + centerOffset.x) / input.TileWidth);
					int row = input.Height - 1 - (int)((obj.Y + centerOffset.y) / input.TileWidth);

					//Debug.Log("obj.X + centerOffset: "+(obj.X+centerOffset.x)+" column: "+col);

					if (typeIndex<robots.Length) {
						GameObject newRobot = Instantiate(robots[typeIndex], new Vector3(col, 0, row), Quaternion.Euler(0, obj.Rotation, 0));
						GridMover mover = newRobot.GetComponent<GridMover>();
						if (mover!=null) {
							mover.speed = robotSpeed;
						}
					}
				}
			}
		}
	}
}
