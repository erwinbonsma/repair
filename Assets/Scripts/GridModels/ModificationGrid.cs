﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModificationGrid : GridModel {
	public GameObject DebugMarker;
	public bool strictPlacement = true;
	public bool RandomInit = true;

	protected override void Initialize() {
		if (!RandomInit)
			return;
		bool[,] baseGrid = new bool[width, height];

		for (int x=0;x<width;x++) {
			for (int y=0;y<height;y++) {
				baseGrid[x, y]=Random.value<0.7f;
			}
		}
		// remove isolated points:
		for (int x=0;x<width;x++) {
			for (int y = 0; y<height; y++) {
				if (baseGrid[x, y]) {
					int connectionsCode = 0;
					if (y<height-1 && baseGrid[x, y+1]) {
						connectionsCode += 1; // North conn possible
					}
					if (x<width-1 && baseGrid[x+1, y]) {
						connectionsCode += 2; // East conn possible
					}
					if (y>0 && baseGrid[x, y-1]) {
						connectionsCode += 4; // South conn possible
					}
					if (x>0 && baseGrid[x-1, y]) {
						connectionsCode += 8; // West conn possible
					}
					if (connectionsCode==0) {
						baseGrid[x, y]=false;
					}
				}
			}
		}

		for (int x = 0; x<width; x++) {
			for (int y = 0; y<height; y++) {
				//Debug.Log("PLACING TILE AT "+x+","+y);
				if (!baseGrid[x,y]) {
					var prefab = GetRandomRepairTilePrefab(0);
					if (prefab!=null) {
						var tile = Instantiate(prefab);
						tile.GetComponent<Connections>().SkipRegister();
						base.PlaceTile(tile, new Vector2Int(x, y),true);
					} else {
						Debug.Log("No tile to place at "+x+","+y);
					}
				} else {
					if (DebugMarker!=null) {
						var marker = Instantiate(DebugMarker);
						marker.transform.position=new Vector3(x, 0.25f, y);
					}

					int connectionsCode = 0;
					if (y<height-1 && baseGrid[x, y+1]) {
						connectionsCode += 1; // North conn possible
					}
					if (x<width-1 && baseGrid[x+1, y]) {
						connectionsCode += 2; // East conn possible
					}
					if (y>0 && baseGrid[x, y-1]) {
						connectionsCode += 4; // South conn possible
					}
					if (x>0 && baseGrid[x-1, y]) {
						connectionsCode += 8; // West conn possible
					}
					var prefab = GetRandomRepairTilePrefab(connectionsCode);
					if (prefab!=null) {
						var tile = Instantiate(prefab);
						tile.GetComponent<Connections>().SkipRegister();
						//Debug.Log("Instantiating "+prefab.name);
						base.PlaceTile(tile, new Vector2Int(x, y),true);
					} else {
						Debug.Log("No tile to place at "+x+","+y);
					}
				}
			}
		}
		//Debug.Log("End of initialize");
	}

	public GameObject GetRandomRepairTilePrefab(Connections desired) {
		return GetRandomRepairTilePrefab(desired.Code);
	}

	public GameObject GetRandomRepairTilePrefab(int connectionsCode) {
		float tileProbSum = 0;
        foreach (var tilePrefab in tileRepairPrefabs) {
			if (tilePrefab.GetComponent<Connections>().Code==connectionsCode) {
				tileProbSum += tilePrefab.GetComponent<TileProbability>().probability;
			}
        }
		if (tileProbSum==0)
			return null; 

        float select = UnityEngine.Random.Range(0, tileProbSum);
        float sumSofar = 0;

        foreach (var tilePrefab in tileRepairPrefabs) {
			if (tilePrefab.GetComponent<Connections>().Code==connectionsCode) {
				sumSofar += tilePrefab.GetComponent<TileProbability>().probability;
				if (sumSofar >= select) {
					return tilePrefab;
				}
			}
        }

        // This should never happen (not even in case of rounding artifacts...)
        Debug.Assert(false);
        return null;
    }

    Connections ConnectionsAt(Vector2Int pos) {
        var tile = TileAt(pos);

        if (tile != null) {
            return tile.GetComponent<Connections>();
        } else {
            return null;
        }
    }

	public override bool CanPlaceTile(GameObject tile, Vector2Int pos) {
		if (!strictPlacement)
			return true;

		int robots = RobotController.main.GetNumRobotsAt(pos);
		return robots==0;
	}

	public void SimpleForcePlaceTile(GameObject tile, Vector2Int pos, bool setPosition = false) {
		base.ForcePlaceTile(tile, pos, setPosition);
	}

	public override void ForcePlaceTile(GameObject tile, Vector2Int pos, bool setPosition=false) {
		/*
		if (TileAt(pos) != null) {
			//Debug.Log("Destroying tile");
			Destroy(TileAt(pos));
        }*/
		base.ForcePlaceTile(tile, pos, true);

		var myCon = tile.GetComponent<Connections>();

		// check left (west)
		Vector2Int nbPos = pos+Vector2Int.left;
        var nbCon = ConnectionsAt(nbPos);
        if (nbCon != null && nbCon.East != myCon.West) {
			int oldCode = nbCon.Code;
			//Debug.Log("Replacing left neighbor");
			nbCon.East=myCon.West;
			//Debug.Log("Old code: "+oldCode+" new Code: "+nbCon.Code);
			ReplaceTile(nbPos, nbCon);
        }
		// check right (east)
		nbPos = pos+Vector2Int.right;
        nbCon = ConnectionsAt(nbPos);
        if (nbCon != null && nbCon.West != myCon.East) {
			int oldCode = nbCon.Code;
			//Debug.Log("Replacing right neighbor");
			nbCon.West=myCon.East;
			//Debug.Log("Old code: "+oldCode+" new Code: "+nbCon.Code);
			ReplaceTile(nbPos, nbCon);
        }
		// check up (north)
		nbPos = pos+Vector2Int.up;
        nbCon = ConnectionsAt(nbPos);
        if (nbCon != null && nbCon.South != myCon.North) {
			int oldCode = nbCon.Code;
			//Debug.Log("Replacing up neighbor");
			nbCon.South=myCon.North;
			//Debug.Log("Old code: "+oldCode+" new Code: "+nbCon.Code);
			ReplaceTile(nbPos, nbCon);
        }
		// check down (south)
		nbPos = pos+Vector2Int.down;
        nbCon = ConnectionsAt(nbPos);
        if (nbCon != null && nbCon.North != myCon.South) {
			int oldCode = nbCon.Code;
			//Debug.Log("Replacing down neighbor");
			nbCon.North=myCon.South;
			//Debug.Log("Old code: "+oldCode+" new Code: "+nbCon.Code);
			ReplaceTile(nbPos, nbCon);
        }
	}

	void ReplaceTile(Vector2Int position, Connections blueprint) {
		GameObject prefab = GetRandomRepairTilePrefab(blueprint);
		if (prefab==null) {
			Debug.Log("Warning: there's no prefab that fits the connections blueprint!");
			return;
		}
		GameObject newTile = Instantiate(prefab);
		newTile.GetComponent<Connections>().SkipRegister();
		//Debug.Log("Destroying tile");

		//Destroy(blueprint.gameObject);

		base.ForcePlaceTile(newTile, position, true);
	}
}
