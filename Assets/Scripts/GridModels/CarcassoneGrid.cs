﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarcassoneGrid : GridModel {

    public GameObject emptyTilePrefab;

    public override void Update() {
        base.Update();

        var tileGrid = GameObject.FindGameObjectWithTag("TileGrid").transform;
        foreach (Vector2Int pos in GridPositions()) {
            if (TileAt(pos) == null) {
                var tile = Instantiate(emptyTilePrefab, tileGrid);
                ForcePlaceTile(tile, pos, true);
            }
        }
    }

    bool TileCompatible(Vector2Int fromPos, Vector2Int dir, bool connectionTowards, ref bool connects) {
        var destPos = fromPos + dir;

        if (!IsInsideGrid(destPos)) {
            // The tile cannot connect towards the border
            return !connectionTowards;
        }

        var tile = TileAt(destPos);
        var connections = tile.GetComponent<Connections>();
        if (!connections.IsEmpty) {
            // There's a non-empty tile. Check if the connections match
            bool hasConnection = false;

            if (dir == Vector2Int.left) {
                hasConnection = connections.East;
            } else if (dir == Vector2Int.right) {
                hasConnection = connections.West;
            } else if (dir == Vector2Int.up) {
                hasConnection = connections.South;
            } else if (dir == Vector2Int.down) {
                hasConnection = connections.North;
            } else {
                Debug.Assert(false);
            }
            if (connectionTowards != hasConnection) {
                return false;
            }

            // This creates a connection
            connects |= hasConnection;
        }

        return true;
    }

    public override bool CanPlaceTile(GameObject tile, Vector2Int pos) {
        var existingTile = TileAt(pos);
        var existingConnections = existingTile.GetComponent<Connections>();
        if (!existingConnections.IsEmpty) {
            return false;
        }

        var myCon = tile.GetComponent<Connections>();
        bool connects = false;

        if (!TileCompatible(pos, Vector2Int.left, myCon.West, ref connects)) {
            return false;
        }
        if (!TileCompatible(pos, Vector2Int.right, myCon.East, ref connects)) {
            return false;
        }
        if (!TileCompatible(pos, Vector2Int.up, myCon.North, ref connects)) {
            return false;
        }
        if (!TileCompatible(pos, Vector2Int.down, myCon.South, ref connects)) {
            return false;
        }

        return connects;
    }
}
