﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTileGrid : GridModel {

    public override bool CanPlaceTile(GameObject tile, Vector2Int pos) {
        return TileAt(pos) == null;
    }
}
