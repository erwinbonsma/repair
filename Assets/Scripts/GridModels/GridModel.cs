﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public abstract class GridModel : MonoBehaviour {

	// Tiles which the user can select and place
	public GameObject[] selectableTilePrefabs;

	// Tiles which may be used to repair the grid
	public GameObject[] tileRepairPrefabs;

	public int width;
	public int height;

	public bool Awoken { get; private set; }

	public static GridModel main {get; private set;}

    GameObject[,] tiles;

    public void Awake() {
        if (main != null) {
			throw new Exception("There cannot be two GridModels in one scene");
        }
        main = this;

        tiles = new GameObject[width, height];

		Initialize();
    }

	protected virtual void Initialize() {

	}

    public virtual void Update() {
        Awoken = true;
    }

	void OnDestroy() {
		main = null;
	}

    public abstract bool CanPlaceTile(GameObject tile, Vector2Int pos);

    public bool CanPlaceTileSomewhere(GameObject tile) {
        Vector2Int pos = Vector2Int.zero;

        for (pos.x = 0; pos.x < width; pos.x++) {
            for (pos.y = 0; pos.y < height; pos.y++) {
                if (CanPlaceTile(tile, pos)) {
                    return true;
                }
            }
        }

        // Debug.Log("Cannot place tile " + tile.name + " somewhere");
        return false;
    }

    public virtual void ForcePlaceTile(GameObject tile, Vector2Int pos, bool updatePosition=false) {
        Debug.Assert(IsInsideGrid(pos));
		if (tiles[pos.x,pos.y]!=null) {
			Destroy(tiles[pos.x, pos.y]);
		}
        tiles[pos.x, pos.y] = tile;
		if (updatePosition) {
			tile.transform.position = new Vector3(pos.x, 0, pos.y);
		}
    }

    public virtual void PlaceTile(GameObject tile, Vector2Int pos, bool updatePosition=false) {
        Debug.Assert(IsInsideGrid(pos));
        if (!CanPlaceTile(tile, pos)) {
            return;
        }
		ForcePlaceTile(tile, pos, updatePosition);
    }

    public bool IsInsideGrid(Vector2Int pos) {
        return pos.x >= 0 && pos.x < width && pos.y >= 0 && pos.y < height;
    }

    public GameObject TileAt(Vector2Int pos) {
        if (!IsInsideGrid(pos)) {
            return null;
        }
        return tiles[pos.x, pos.y];
    }

    public IEnumerable<Vector2Int> GridPositions() {
        Vector2Int pos = Vector2Int.zero;
        for (pos.x = 0; pos.x < width; pos.x++) {
            for (pos.y = 0; pos.y < height; pos.y++) {
                yield return pos;
            }
        }
    }
}
