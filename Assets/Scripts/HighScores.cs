﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// This is not stored between sessions. 
// If you wish to store scores between sessions, use Unity's PlayerPrefs?
public static class HighScores 
{
	//static Dictionary<int, int> levelHighScore;

	static string GetKey(int levelNum) {
		return "hiscore"+levelNum;
	}

	public static int GetHighscore(int levelNum) {
		//if (levelHighScore==null) {
		//	levelHighScore=new Dictionary<int, int>();
		//}
		string key = GetKey(levelNum);
		if (PlayerPrefs.HasKey(key)) {
			return PlayerPrefs.GetInt(key);
		} else {
			return 0;
		}
	}

	public static bool IsNewHighscore(int newScore, int levelNum) {
		string key = GetKey(levelNum);
		//if (levelHighScore==null) {
		//	levelHighScore=new Dictionary<int, int>();
		//}
		if (!PlayerPrefs.HasKey(key) || newScore>PlayerPrefs.GetInt(key)) { 
			PlayerPrefs.SetInt(key,newScore);
			PlayerPrefs.Save();
			return true;
		} 
		return false;
	}

	public static void ClearAllScores() {
		PlayerPrefs.DeleteAll();
	}
}
