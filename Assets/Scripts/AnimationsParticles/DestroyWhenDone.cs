﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Usage:
// Intended for one-shot audio and or particle effects.
// Add this to an instantiated prefab that plays audio and/or particles on Awake.
// This script makes sure that the game object is removed when the effects are done.
public class DestroyWhenDone : MonoBehaviour
{
	AudioSource audioSource;
	ParticleSystem particles;

    void Start()
    {
		audioSource = GetComponent<AudioSource>();
		particles = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (audioSource!=null && !audioSource.isPlaying) {
			audioSource=null;
		}
        if (particles!=null && !particles.isPlaying) {
			particles=null;
		}

		if (audioSource==null && particles==null) {
			Destroy(gameObject);
		}
    }
}
