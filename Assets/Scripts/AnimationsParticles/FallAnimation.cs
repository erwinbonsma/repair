﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallAnimation : MonoBehaviour
{
	public Animator anim;
	public bool Fallen { get; private set; } = false;

	public void PlayAnimation(bool forward=true) {
		if (forward) {
			anim.SetTrigger("FallForward");
		} else {
			anim.SetTrigger("FallBackward");
		}
		Fallen=true;
	}
	/*
    void Update()
    {
		// test:
		if (Input.GetKeyDown(KeyCode.F2)) {
			PlayAnimation();
		}        
    }
	*/
}
