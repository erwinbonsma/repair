﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectAnimation : MonoBehaviour
{
	public Animator anim;

	public void PlayAnimation() {
		anim.SetTrigger("Connect");
	}

    void Update()
    {
		// test:
		if (Input.GetKeyDown(KeyCode.F1)) {
			PlayAnimation();
		}        
    }
}
