﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RobotController : MonoBehaviour {
	public static RobotController main { get; private set;}

	public Action<GridMover, GridMover> onRobotsMeeting;
	public Action<GridMover> onRobotAdded;

	List<GridMover> robots;

	public int NumRobots {
		get {
			return robots.Count;
		}
	}

	private void Awake() {
		if (main!=null) {
			throw new Exception("There can only be one RobotController in the scene.");
		}
		main=this;

		robots=new List<GridMover>();
	}

	private void OnDestroy() {
		main=null;
	}

	public void RegisterRobot(GridMover robot) {
		robots.Add(robot);

		robot.onCenterPass += OnCenterPass;
		robot.onBoundaryPass += OnBoundaryPass;

		if (onRobotAdded != null) {
			onRobotAdded(robot);
		}
	}

	void OnCenterPass(GridMover robot) {
		// Debug.Log(robot.ToString() + " passed center");

		// Check if another robot is at my next tile and moving to my tile
		Vector2Int nextPos = robot.Position + robot.Direction;
		foreach (GridMover otherBot in GetRobotsAt(nextPos, robot)) {
			if (otherBot.Position + otherBot.Direction == robot.Position) {
				//Debug.Log(robot.ToString() + " and " + otherBot + " meet at boundary of " + robot.Position + " and " + otherBot.Position);

				if (onRobotsMeeting != null) {
					onRobotsMeeting(robot, otherBot);
				}
			}
		}
	}

	void OnBoundaryPass(GridMover robot) {
		// Debug.Log(robot.ToString() + " passed boundary");

		// Check if another robot is entering (or just entered) the same tile
		foreach (GridMover otherBot in GetRobotsAt(robot.Position, robot)) {
			if (otherBot.IsMovingTowards(robot.Position)) {
				//Debug.Log(robot.ToString() + " and " + otherBot + " meet at center of " + robot.Position);

				if (onRobotsMeeting != null) {
					onRobotsMeeting(robot, otherBot);
				}
			}
		}
	}

	// Memory + code efficiency chosen over time efficiency. 
	// Reason: I don't expect huge lists here, and this method shouldn't be called every frame.
	public int GetNumRobotsAt(Vector2Int pos, GridMover exclude=null) {
		int total = 0;
		foreach (GridMover robot in GetRobotsAt(pos, exclude)) {
			total++;
		}
		return total;
	}

	public IEnumerable<GridMover> GetRobotsAt(Vector2Int pos, GridMover exclude=null) {
		foreach (GridMover robot in robots) {
			if (robot != exclude && robot.Position == pos) {
				yield return robot;
			}
		}
	}

	public IEnumerable<GridMover> Robots() {
		return robots;
	}
}
