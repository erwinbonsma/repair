﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour {
    void Start() {
		var playerController = GetComponent<PlayerControl>();

        playerController.onCursorMove += OnCursorMove;
		Vector2Int startPos = new Vector2Int(
			Mathf.RoundToInt(transform.position.x), 
			Mathf.RoundToInt(transform.position.z)
		);
		playerController.Cursor = startPos;
    }

    void OnCursorMove(PlayerControl control) {
        transform.position = new Vector3(control.Cursor.x, 0, control.Cursor.y);
    }
}
