﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class MoveAnimator {
	protected GridMover mover;
	protected Vector3 startPos, endPos;
	protected Quaternion startDir, endDir;

	public MoveAnimator(GridMover moverVal) {
		mover = moverVal;

		startPos = moverVal.transform.position;
		endPos = new Vector3(
			mover.Position.x + mover.Direction.x * 0.5f,
			0,
			mover.Position.y + mover.Direction.y * 0.5f
		);

		startDir = moverVal.transform.rotation;
		endDir = Quaternion.LookRotation(new Vector3(mover.Direction.x, 0, mover.Direction.y));
	}

	public virtual void Update() {
		mover.transform.position = Vector3.Lerp(startPos, endPos, mover.Progress);
	}
}

class MoveViaCenterAnimator : MoveAnimator {
	protected Vector3 midPos;

	private bool passedCenter = false;

	public MoveViaCenterAnimator(GridMover moverVal) : base(moverVal) {
		midPos = new Vector3(mover.Position.x, 0, mover.Position.y);
	}

	public virtual void PassedCenter() {
		mover.transform.rotation = endDir;
	}

	public override void Update() {
		if (mover.Progress < 0.5f) {
			mover.transform.position = Vector3.Lerp(startPos, midPos, mover.Progress * 2);
		} else {
			if (!passedCenter) {
				passedCenter = true;
				PassedCenter();
			}
			mover.transform.position = Vector3.Lerp(midPos, endPos, (mover.Progress - 0.5f) * 2);
		}
	}
}

class HardTurnAnimator : MoveViaCenterAnimator {

	private bool finishedTurn;

	public HardTurnAnimator(GridMover moverVal) : base(moverVal) {}

	public override void PassedCenter() {}

	public override void Update() {
		base.Update();

		if (mover.Progress > 0.4f && !finishedTurn) {
			mover.transform.rotation = Quaternion.Lerp(startDir, endDir, (mover.Progress - 0.4f) * 5);
			if (mover.Progress >= 0.6f) {
				finishedTurn = true;
			}
		}
	}
}

class SmoothTurnAnimator : MoveAnimator {
	Quaternion midDir;

	public SmoothTurnAnimator(GridMover moverVal) : base(moverVal) {
		midDir = Quaternion.Lerp(startDir, endDir, 0.5f);
	}

	public override void Update() {
		base.Update();

		if (mover.Progress < 0.1f) {
			mover.transform.rotation = Quaternion.Lerp(startDir, midDir, mover.Progress * 10);
		} else if (mover.Progress >= 0.9f) {
			mover.transform.rotation = Quaternion.Lerp(midDir, endDir, (mover.Progress - 0.9f) * 10);
		}
	}
}

public class GridMover : MonoBehaviour {
	public static float turboMultiplier = 1;

	public int pairType;
	public float speed; // in tiles per second

	public float textureScrollMultiplier = 1;
	public MeshRenderer LeftWheel;
	public MeshRenderer RightWheel;
	float VLeft=0;
	float VRight=0;

	public Action<GridMover> onCenterPass;
	public Action<GridMover> onBoundaryPass;
	public Action<GridMover> onHardBump;

	public Vector2Int Direction { get; private set; }
	public Vector2Int Position { get; private set; }

	MoveAnimator moveAnimator;

	// A value between 0 and 1. Progress is 0.5 when it is at the center of a tile and 0 when it is at the boundary.
	public float Progress { get; private set; }

	public bool IsMovingTowards(Vector2Int pos) {
		if (Progress < 0.5f) {
			return pos == Position;
		} else {
			return pos == (Position + Direction);
		}
	}

	void Start () {
		Position = new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z));
		int angle = Mathf.RoundToInt(transform.localEulerAngles.y / 90);
		switch (angle) {
			case 0:
			case 4: Direction = Vector2Int.up; break;
			case 1: Direction = Vector2Int.right; break;
			case 2: Direction = Vector2Int.down; break;
			case 3: Direction = Vector2Int.left; break;
			default: Debug.Assert(false); break;
		}

		Progress = 0.5f;
		RobotController.main.RegisterRobot(this);
		moveAnimator = new MoveAnimator(this);
	}

	Connections GetConnectionsAt(Vector2Int pos) {
		var tile = GridModel.main.TileAt(pos);
		return tile != null ? tile.GetComponent<Connections>() : null;
	}

	void ScrollTexture() {
		VLeft =(VLeft+(Time.deltaTime * speed * textureScrollMultiplier))%1;
		VRight=(VRight+(Time.deltaTime * speed * textureScrollMultiplier))%1;
		if (LeftWheel!=null) {
			LeftWheel.material.SetFloat("_VScroll", VLeft);
		}
		if (RightWheel!=null) {
			RightWheel.material.SetFloat("_VScroll", VRight);
		}
	}
	
	MoveAnimator HandleBoundaryPass() {
		Vector2Int nextPos = Position + Direction;
		Connections next = GetConnectionsAt(nextPos);

		if (next == null || !next.InDirectionPossible(Direction)) {
			// Hard bump. No connection.

			if (onHardBump != null) {
				onHardBump(this);
			}

			nextPos = Position;
			Direction *= -1;
			transform.rotation = Quaternion.LookRotation(new Vector3(Direction.x, 0, Direction.y));
		}

		Position = nextPos;
		Connections current = GetConnectionsAt(Position);
		Director director = current.transform.GetComponent<Director>();
		Vector2 oldDir = Direction;
		Direction = director.GetNewDirection(Direction);

		// Notify observers now that all external status is updated
		if (onBoundaryPass != null) {
			onBoundaryPass(this);
		}

		// Initiate movement animation
		if (Direction == oldDir) {
			// Move straight
			return new MoveAnimator(this);
		} else if (Direction + oldDir == Vector2.zero) {
			// Dead end
			return new MoveViaCenterAnimator(this);
		} else {
			// Turn
			if (director.HardTurns) {
				return new HardTurnAnimator(this);
			} else {
				return new SmoothTurnAnimator(this);
			}
		}
	}

	void Update() {
		if (speed == 0) {
			return;
		}
		ScrollTexture();

		bool beforeCenter = Progress < 0.5f;

		Progress += Time.deltaTime * speed * turboMultiplier;

		if (beforeCenter && Progress >= 0.5f) {
			if (onCenterPass != null) {
				onCenterPass(this);
			}
		}

		if (Progress >= 1.0f) {
			var newProgress = Progress - 1;

			// Wrap up previous animation (to ensure that any turns are fully completed)
			Progress = 1;
			moveAnimator.Update();

			Progress = newProgress;
			moveAnimator = HandleBoundaryPass();
		}

		if (moveAnimator != null) {
			moveAnimator.Update();
		}
	}
}
