﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

public class Logger {
	const int LogLevel = 2;
	public static bool Initialized {
		get {
			return instance!=null;
		}
	}

	static Logger instance = null;
	public static Logger Instance {
		get {
			if (instance == null)
				instance = new Logger();
			return instance;
		}
	}

	StreamWriter writefile;

	public Logger(string filename="log.txt") {
		if (LogLevel==0 || Application.platform==RuntimePlatform.WebGLPlayer)
			return;
		writefile = new StreamWriter(File.Open(filename, FileMode.Append));
		DateTime now = DateTime.Now;
		writefile.WriteLine(now.ToLocalTime());
		//writefile.WriteLine(now.ToShortDateString() + " " + now.ToLocalTime());
	}

	public void Write(string line, int detailLevel=1) {
		if (detailLevel > LogLevel)
			return;
		int min = (int)(Time.time / 60);
		int sec = (int)(Time.time % 60);
		writefile.WriteLine(String.Format("{0,3}",min)+":"+String.Format("{0:00}",sec) + " : "+ line);
		writefile.Flush();
	}

	public void Close() {		
		writefile.Close();
		instance=null;
	}
}
