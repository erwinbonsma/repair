﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsScreenControl : ReturnToMainMenu {

    void Update() {
        if (Input.anyKeyDown) {
            SceneManager.LoadSceneAsync("MainMenu");
        }
    }
}
