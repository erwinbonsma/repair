﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToMainMenu : MonoBehaviour {

    bool ignoreKey;

    void Start() {
        // If key is pressed from start, ignore it to avoid instantaneous return to main menu
        ignoreKey = Input.anyKey;
    }

    void Update() {
        if (Input.anyKeyDown) {
            if (!ignoreKey) {
                SceneManager.LoadSceneAsync("MainMenu");
            }
        } else {
            ignoreKey = false;
        }
    }
}
