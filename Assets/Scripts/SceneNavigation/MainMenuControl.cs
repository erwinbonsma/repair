﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuControl : MonoBehaviour {

	static bool hardMode = false;

	public TextMeshProUGUI difficultyButtonText;

	private void Awake() {
		if (hardMode) {
			difficultyButtonText.text = "Expert Mode";
		}
	}

	private void Update() {
		if (Input.GetKeyDown(KeyCode.F5) && Input.GetKey(KeyCode.LeftAlt)) {
			Debug.Log("Clearing all high scores");
			HighScores.ClearAllScores();
		}
	}

	public void StartCarcassoneLevel() {
		DefaultGameControl.StartGame(hardMode?0.12f:0.08f,"CarcassoneGrid",
			GameControl.CORNER + GameControl.STRAIGHT + GameControl.CROSSING + 
			GameControl.VSPLIT + GameControl.DEADEND
		);
    }

	/*
    public void StartCarcassoneTestLevel() {
        SceneManager.LoadSceneAsync("CarcassoneTestGrid");
    }

    public void Start1PLevel() {
		DefaultGameControl.StartGame(1,"SinglePlayerDesigned");
    }

	public void Start1PRandom() {
		DefaultGameControl.StartGame(1,"SinglePlayerRandomVariant");
    }

	public void Start1PExpert() {
		DefaultGameControl.StartGame(0.35f,"SinglePlayerHard");
    }
	*/

	public void Start2PCoop() {
		if (hardMode) {
			InfiniteGameControl.StartGame("MultiPlayerCoop", 1.3f,3,2,2);
		} else {
			InfiniteGameControl.StartGame("MultiPlayerCoop", 1.2f);
		}
    }

	public void StartMultiplayer() {
		if (hardMode) {
			DefaultGameControl.StartGame(0.6f,"MultiPlayer");
		} else {
			DefaultGameControl.StartGame(0.4f, "MultiPlayerSimple");
		}
    }

	public void StartInfiniteGame() {
		if (hardMode) {
			InfiniteGameControl.StartGame("SinglePlayerDesigned",1.1f, 3, 2, 2);
		} else {
			InfiniteGameControl.StartGame();
		}
    }

	public void ToggleDifficulty() {
		hardMode^=true;
		if (hardMode) {
			difficultyButtonText.text="Expert Mode";
		} else {
			difficultyButtonText.text="Beginner Mode";
		}
	}

	// ---------------------------------------------------------------------------------------

	public void ShowHelp() {
        SceneManager.LoadSceneAsync("HelpScreen");
    }

    public void ShowCredits() {
        SceneManager.LoadSceneAsync("CreditsScreen");
    }
}
