﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarcassoneLevelControl : LevelControl {

    public float maxBotSpeed;
    public float botAccelleration;

    override protected void OnGridComplete(PlayerControl playerControl) {
        Debug.Log("Grid complete!");
        StartCoroutine(SpeedUpBots());
    }

    override protected void OnRobotsMeeting(GridMover bot1, GridMover bot2) {
        bot1.speed = 0;
        bot2.speed = 0;

        if (levelDone) {
            return;
        }
        levelDone = true;

		AnimateRobotConnect(bot1.gameObject, bot2.gameObject);

		Vector3 midPos = (bot1.transform.position + bot2.transform.position) / 2;
        StartCoroutine(ZoomTo(midPos));

        StartCoroutine(LevelComplete());
    }

    override protected void OnHardBump(GridMover bot) {
        bot.speed = 0;
		bot.GetComponent<FallAnimation>().PlayAnimation(false);

        if (levelDone) {
            return;
        }
        levelDone = true;

        StartCoroutine(ZoomTo(bot.transform.position));
        StartCoroutine(GameOver());
    }

    IEnumerator SpeedUpBots() {
        while (true) {
            bool accellerating = false;
            foreach (var bot in robotController.Robots()) {
                if (bot.speed > 0 && bot.speed < maxBotSpeed) {
                    bot.speed *= botAccelleration;
                    accellerating = true;
                }
            }

            if (accellerating) {
                yield return new WaitForSeconds(0.2f);
            } else {
                yield break;
            }
        }
    }
}
