﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerLevelControl : LevelControl {
    override protected void OnRobotsMeeting(GridMover bot1, GridMover bot2) {
		if (bot1.pairType==bot2.pairType) {
			bot1.speed = 0;
			bot2.speed = 0;

	        if (levelDone) {
                return;
            }
            levelDone = true;
			ShowText("Player "+(bot1.pairType+1)+" wins!");
			AnimateRobotConnect(bot1.gameObject, bot2.gameObject);

			Vector3 midPos = (bot1.transform.position + bot2.transform.position) / 2;
			StartCoroutine(ZoomTo(midPos));

			// TODO: particle effect oid
			StartCoroutine(LevelComplete());
		} // else do nothing
    }

	public override void ShowEndScreen(bool win) {
		// nothing - OnRobotsMeeting already show the winner!
	}
}
