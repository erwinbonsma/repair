﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelControl : MonoBehaviour {
	public static bool turboPressed=false;

	public AudioClip WinSound;
	public AudioClip LoseSound;

    protected RobotController robotController;
    protected bool levelDone; // Should be set to "true" as soon as a level termination criterion is met

	protected float startTime;

    void Start() {
        foreach (GameObject playerControlObject in GameObject.FindGameObjectsWithTag("PlayerControl")) {
            var playerControl = playerControlObject.GetComponent<PlayerControl>();
            playerControl.onGridComplete += OnGridComplete;
        }

        robotController = RobotController.main;
        robotController.onRobotsMeeting += OnRobotsMeeting;
        robotController.onRobotAdded += OnRobotAdded;

        foreach (GridMover robot in robotController.Robots()) {
            robot.onHardBump += OnHardBump;
        }
		startTime = Time.time;

		GridMover.turboMultiplier=1;
    }

    void OnRobotAdded(GridMover robot) {
        robot.onHardBump += OnHardBump;
    }

    void Update() {
        if (levelDone) {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            levelDone = true;
			if (Logger.Initialized) {
				Logger.Instance.Close();
			}
            SceneManager.LoadSceneAsync("MainMenu");
        }

		if (turboPressed) {
			if (GridMover.turboMultiplier<4) {
				GridMover.turboMultiplier+=Time.deltaTime*4;
			}
			if (GridMover.turboMultiplier>4) {
				GridMover.turboMultiplier=4;
			}
		} else {
			if (GridMover.turboMultiplier>1) {
				GridMover.turboMultiplier-=Time.deltaTime*4;
			}
			if (GridMover.turboMultiplier<1) {
				GridMover.turboMultiplier=1;
			}
		}
		turboPressed = false;
    }

	private void OnApplicationQuit() {
		if (Logger.Initialized) {
			Logger.Instance.Close();
		}
	}

	protected virtual void OnGridComplete(PlayerControl playerControl) {}

    protected virtual void OnRobotsMeeting(GridMover bot1, GridMover bot2) {}

    protected virtual void OnHardBump(GridMover bot) {}

    public void ShowText(string text) {
        var panelTransform = transform.Find("Panel");
        var textTransform = panelTransform.transform.Find("MainText");
        var panelText = textTransform.gameObject.GetComponent<TextMeshProUGUI>();

        panelTransform.gameObject.SetActive(true);
        panelText.text = text;
    }

	public void ShowMoveInfo(string text) {
        var panelTransform = transform.Find("ExtraPanel");
        var textTransform = panelTransform.transform.Find("MovesInfo");
        var panelText = textTransform.gameObject.GetComponent<TextMeshProUGUI>();

        panelTransform.gameObject.SetActive(true);
		textTransform.gameObject.SetActive(true);
        panelText.text = text;
	}

	public void ShowScoreInfo(string text) {
        var panelTransform = transform.Find("ExtraPanel");
        var textTransform = panelTransform.transform.Find("ScoreInfo");
        var panelText = textTransform.gameObject.GetComponent<TextMeshProUGUI>();

        panelTransform.gameObject.SetActive(true);
		textTransform.gameObject.SetActive(true);
        panelText.text = text;
	}

	public void ShowHighScoreInfo(string text) {
        var panelTransform = transform.Find("ExtraPanel");
        var textTransform = panelTransform.transform.Find("HighScoreInfo");
        var panelText = textTransform.gameObject.GetComponent<TextMeshProUGUI>();

        panelTransform.gameObject.SetActive(true);
		textTransform.gameObject.SetActive(true);
        panelText.text = text;
	}


    protected IEnumerator ZoomTo(Vector3 pos) {
        var cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
        Camera camera = cameraObject.GetComponent<Camera>();

        Vector3 oldPos = cameraObject.transform.position + cameraObject.transform.forward * 10;
        Vector3 newPos = pos + new Vector3(0.5f, 1f, 0.5f);
        float oldFov = camera.fieldOfView;

        float progress = 0;
        while (progress < 1) {
            progress += 0.01f;

            cameraObject.transform.LookAt(Vector3.Lerp(oldPos, pos, progress));
            camera.fieldOfView = Mathf.Lerp(oldFov, 20, progress);
            yield return new WaitForSeconds(0.02f);
        }
    }

    // Invoke as Co-routine
    protected IEnumerator GameOver() {
		GameControl gameControl = GameControl.currentGame;
		if (gameControl!=null) {
			gameControl.CalculateUpdateShowScore(CountTotalMoves(), Time.time - startTime, false, this);
		}
		GetComponent<AudioSource>().PlayOneShot(LoseSound);
		// TODO: particle effect on robots

        yield return new WaitForSeconds(5f);

		LoadNextScene();
    }

	protected int CountTotalMoves() {
		PlayerControl[] player = FindObjectsOfType<PlayerControl>();
		int moves = 0;
		foreach (var play in player) {
			moves += play.MovesMade;
		}
		return moves;
	}

    // Invoke as Co-routine
    protected IEnumerator LevelComplete(float delay=5) {
		GameControl gameControl = GameControl.currentGame;
		if (gameControl!=null) {
			gameControl.CalculateUpdateShowScore(CountTotalMoves(), Time.time - startTime, true, this);
		}

        //Debug.Log(endText);
        //ShowText(endText);

		GetComponent<AudioSource>().PlayOneShot(WinSound);

        yield return new WaitForSeconds(delay);

		LoadNextScene();
    }

	public virtual void ShowEndScreen(bool win) {
		if (win) {
			ShowText("RE-PAIRED!");
		} else {
			ShowText("GAME OVER");
		}
	}

	void LoadNextScene() {
		GameControl gameControl = GameControl.currentGame;
		if (gameControl!=null) {
			gameControl.LoadNextMap();
		} else {
			SceneManager.LoadSceneAsync("MainMenu");
		}
	}

	protected void AnimateRobotConnect(GameObject robot1, GameObject robot2) {
		robot1.GetComponent<ConnectAnimation>().PlayAnimation();
		robot2.GetComponent<ConnectAnimation>().PlayAnimation();

		Vector3 difference = robot2.transform.position-robot1.transform.position;
		Quaternion targetRotation1 = Quaternion.LookRotation(difference);
		Quaternion targetRotation2 = Quaternion.LookRotation(-difference);

		float targetDistance = 0.445f; // it's magic! :-D

		Vector3 targetPosition1 = robot1.transform.position + difference * (difference.magnitude - targetDistance)/(2*difference.magnitude);
		Vector3 targetPosition2 = robot2.transform.position - difference * (difference.magnitude - targetDistance)/(2*difference.magnitude);

		StartCoroutine(RotateRobot(robot1, robot1.transform.rotation, targetRotation1, robot1.transform.position, targetPosition1));
		StartCoroutine(RotateRobot(robot2, robot2.transform.rotation, targetRotation2, robot2.transform.position, targetPosition2));
	}

	IEnumerator RotateRobot(GameObject target, Quaternion start, Quaternion end, Vector3 startPos, Vector3 endPos) {
		int steps = 20;
		for (int i=0;i<steps;i++) {			
			target.transform.rotation=Quaternion.Slerp(start, end, i*1f/20);
			target.transform.position=Vector3.Lerp(startPos, endPos, i*1f/20);
			yield return new WaitForSeconds(0.05f);
		}
	}
}
