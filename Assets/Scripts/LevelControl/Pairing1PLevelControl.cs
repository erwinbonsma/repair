﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pairing1PLevelControl : LevelControl {
	public GameObject crashEffect;

	int matches = 0;

    override protected void OnRobotsMeeting(GridMover bot1, GridMover bot2) {
        bot1.speed = 0;
        bot2.speed = 0;
		if (bot1.pairType==bot2.pairType) {
			//Debug.Log("Pair!");
			if (!bot1.GetComponent<FallAnimation>().Fallen && !bot2.GetComponent<FallAnimation>().Fallen) {
				AnimateRobotConnect(bot1.gameObject, bot2.gameObject);
			}
			// TODO: particle effect oid
			matches++;
			if (matches==RobotController.main.NumRobots/2) {
				if (levelDone) {
					return;
				}
				levelDone = true;

				Vector3 midPos = (bot1.transform.position + bot2.transform.position) / 2;
				StartCoroutine(ZoomTo(midPos));

				StartCoroutine(LevelComplete(8));
			}
		} else {
			bot1.GetComponent<FallAnimation>().PlayAnimation(false);
			bot2.GetComponent<FallAnimation>().PlayAnimation(false);

			if (crashEffect!=null) {
				Instantiate(crashEffect, (bot1.transform.position + bot2.transform.position)/2, Quaternion.identity);
			}

			if (levelDone) {
				return;
			}
			levelDone = true;

			//Debug.Log("Lose!");

			Vector3 midPos = (bot1.transform.position + bot2.transform.position) / 2;
			StartCoroutine(ZoomTo(midPos));

	        StartCoroutine(GameOver());
		}
    }

	public override void ShowEndScreen(bool win) {
		GameControl current = GameControl.currentGame;

		int totalMoves = CountTotalMoves();
		float timeTaken = Time.time - startTime;
		int levelScore = 0;
		if (win) {
			levelScore = 1000000 / (int)(10000000 / ((10+totalMoves) * timeTaken));
		}

		int levelNum = SceneManager.GetActiveScene().buildIndex;

		string endText = "RE-PAIRED!";
		if (win && HighScores.IsNewHighscore(levelScore, levelNum)) {
			endText="NEW HIGHSCORE!";
		}
		if (!win) {
			endText = "GAME OVER!";
		}
		ShowText(endText);
		ShowScoreInfo("score: "+levelScore);
		
		ShowMoveInfo("moves: "+totalMoves+" time: "+Mathf.RoundToInt(timeTaken));
		ShowHighScoreInfo("high score: "+HighScores.GetHighscore(levelNum));
	}	
}
